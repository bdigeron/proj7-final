import os
import csv
import flask
import json
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import dateutil.parser as parser
import logging
from flask import Flask, redirect, url_for, request, render_template, jsonify, session, Response, request
from flask_restful import Resource, Api, request
from pymongo import MongoClient
import logging
from flask_wtf import FlaskForm
from functools import wraps
from wtforms import StringField, PasswordField, BooleanField, validators
import password
from itsdangerous import (TimedJSONWebSignatureSerializer \
                                  as Serializer, BadSignature, \
                                  SignatureExpired)
import time
from flask_login import (LoginManager, current_user, login_required,
                            login_user, logout_user, UserMixin, 
                            confirm_login, fresh_login_required)

idNum = 0
USERS = {}


app = Flask(__name__)
api = Api(app)
app.config['SECRET_KEY'] = "secretkey"

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb
db1 = client.userdb



# your user class 
class User(UserMixin):
    def __init__(self, name, id, active=True):
        self.name = name
        self.id = id
        self.active = active

    def is_active(self):
        return self.active

# step 1 in slides
login_manager = LoginManager()

# step 6 in the slides
login_manager.login_view = "login"
login_manager.login_message = u"Please log in to access this page."
login_manager.refresh_view = "reauth"

# step 2 in slides 
@login_manager.user_loader
def load_user(id):
    return USERS.get(int(id))

login_manager.setup_app(app)



@app.route("/")
@app.route("/index")
@login_required
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')

class registerForm(FlaskForm):
    username = StringField("username", [validators.Length(min=3)])
    password = PasswordField("password", [validators.Length(min=3)])

class loginForm(FlaskForm):
    username = StringField("username", [validators.Length(min=3)])
    password = PasswordField("password", [validators.Length(min=3)])
    rememberMe = BooleanField("remember me")

@app.route("/register", methods = ['GET', 'POST'])
def registerWebsite():
    form = registerForm()
    username = form.username.data
    if request.method == 'POST' and form.validate_on_submit() and db1.userdb.find({"username":username}).count() == 0:
        hashedPassword = password.hash_password(form.password.data)
        global idNum
        global USERS
        idNum = idNum + 1
        idString = str(idNum)
        user = User(username, idString)
        USERS[idNum] = user

        new_user = {
            'id':idString,
            'username':username,
            'password': hashedPassword
        }

        token = generate_auth_token(600, idString)
        session['api_session_token'] = token
        db1.userdb.insert_one(new_user)


        response = jsonify({'username':username})
        response.status_code = 201
        response.headers['location'] = 'localhost:5000/api/users' + idString
        response.autocorrect_location_header = False

        login_user(user)

        return response


    return flask.render_template("register.html", form = form), 400


@app.route("/api/register", methods = ['GET', 'POST'])
def register():
    username = request.json.get('username')
    if db1.userdb.find({"username":username}).count() == 0:
        hashedPassword = password.hash_password(request.json.get('password'))
        global idNum
        global USERS
        idNum = idNum + 1
        idString = str(idNum)
        user = User(username, idString)
        USERS[idNum] = user

        new_user = {
            'id':idString,
            'username':username,
            'password': hashedPassword
        }

        token = generate_auth_token(600, idString)
        session['api_session_token'] = token
        db1.userdb.insert_one(new_user)


        response = jsonify({'username':username})
        response.status_code = 201
        response.headers['location'] = 'localhost:5000/api/users/' + idString
        response.autocorrect_location_header = False

        login_user(user)

        return response
    else:
        return "please register again", 400




@app.route("/loginform", methods = ['GET', 'POST'])
def loginFormClass():
    form = loginForm()
    username = form.username.data
    rememberMe = form.rememberMe.data

    if(db1.userdb.find({"username":username}).count() == 0):
        return flask.render_template("login.html", form = form)
    else:
        user = db1.userdb.find({"username":username})
        for value in user:
            hashedPassword = value['password']
            if(password.verify_password(form.password.data, hashedPassword)):
                global idNum
                idString = str(idNum)
                token = generate_auth_token(600, idString)
                session['api_session_token'] = token
                login_user(USERS.get(int(value['id'])), remember = rememberMe)
                return redirect(url_for('index'))
                
            else:
                return flask.render_template("login.html", form = form)

@app.route("/login", methods = ['GET', 'POST'])
def login():
    form = loginForm()
    return flask.render_template("login.html", form = form)

def auth():
    auth = request.authorization
    username = auth.get("username")
    password1 = auth.get("password")
    if db1.userdb.find({"username":username}).count() != 0:
        user = db1.userdb.find({"username":username})
        for value in user:
            hashedPassword = value['password']
            if(password.verify_password(password1, hashedPassword)):
                del(password1)
                return True;
    return False

@app.route("/api/token", methods = ['GET'])
def getToken():
    app.logger.debug(request.authorization)
    if(request.authorization != None):
        authorization = auth()
        if(authorization == True):
            global idNum
            idString = str(idNum)
            token = generate_auth_token(600, idString).decode("utf-8", errors = "ignore")
            session['api_session_token'] = token
            response = jsonify({"token": str(token), "duration": 600})
            return response
        else:
            return "Please register or login first to get a token", 401
    else:
        if(current_user.is_anonymous == False):
            idString = str(idNum)
            token = generate_auth_token(600, idString)
            session['api_session_token'] = token
            response = jsonify({"token": str(token), "duration": 600})
            return response
        else:
            return "Please register or login first to get a token", 401


    

#########Security##########
def generate_auth_token(expiration, id):
   s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
   # pass index of user
   return s.dumps({'id': id})

def verify_auth_token(token):
    s = Serializer(app.config['SECRET_KEY'])
    try:
        data = s.loads(token)
    except SignatureExpired:
        return None    # valid token, but expired
    except BadSignature:
        return None    # invalid token
    return "Success"        



#########Calc html page##########

@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    distance = request.args.get('distance', type = int)
    begin_date = request.args.get('begin_date', type = str)
    begin_time = request.args.get('begin_time', type = str)
    dateAndTime = begin_date + " " + begin_time
    time = arrow.get(dateAndTime, 'YYYY-MM-DD HH:mm')
    
    open_time = acp_times.open_time(km, distance, time.isoformat())
    close_time = acp_times.close_time(km, distance, time.isoformat())
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)



@app.route('/new', methods=['POST'])
def new():

    #error checking for km values if all pass then appending to kmArray
    db.tododb.remove({})
    f = request.form
    distance = int(f['distance'])
    kmArray = []
    for value in f.getlist("km"):
        if(value != ""):
            floatValue = float(value)
            if(floatValue < 0):
                return flask.render_template('negative.html')
            elif(floatValue > 1000):
                return flask.render_template('max.html')    
            elif(floatValue > (distance * 1.2)):
                return flask.render_template('overtwenty.html')
            else:
                kmArray.append(value)
        else:
            break

    if(len(kmArray) == 0):
        return flask.render_template('error.html')
    #loop through all values of keys open and close and add to respective array
    openArray = getValues("open")
    closeArray = getValues("close")


    #create db entries with km distance, open and close times
    for i in range(len(kmArray)):
        # error check here for too fast of input
        item_doc = {
            'distance':request.form['distance'],
            'km': kmArray[i],
            'open':openArray[i],
            'close':closeArray[i]
        }
        db.tododb.insert_one(item_doc)

    return redirect(url_for('index'))


def getValues(key):
    f = request.form
    tempArray = []
    for value in f.getlist(key):
        if(value != ""):
            tempArray.append(value)
    return tempArray


@app.route('/results', methods=['POST'])
def results():
    _items = db.tododb.find()
    items = [item for item in _items]
    if (len(items) == 0):
        return flask.render_template('error.html')
    return render_template('results.html', items=items)

@app.route('/logout', methods = ['GET', 'POST'])
@login_required
def logout():
    session['api_session_token'] = ''
    logout_user()
    return "You have been logged out"

#########API##########


class listAll(Resource):
    def get(self):
        app.logger.debug(request.authorization)
        if(request.authorization != None):
            token = request.authorization.get("username").encode("utf-8", errors = "ignore")
            if(verify_auth_token(token) == "Success"):
                return flask.jsonify(result = toJson("both"))
            else:
                response = jsonify("Please ensure that you have a valid token before accessing the api")
                response.status_code = 401
                return response  
        elif verify_auth_token(session['api_session_token']) == "Success":
            return flask.jsonify(result = toJson("both"))
        else:
            response = jsonify("Please ensure that you have a valid token before accessing the api")
            response.status_code = 401
            return response  


class listOpenOnly(Resource):
    def get(self):
        if(request.authorization != None):
            token = request.authorization.get("username").encode("utf-8", errors = "ignore")
            if(verify_auth_token(token) == "Success"):
                return flask.jsonify(result = toJson("open"))
            else:
                response = jsonify("Please ensure that you have a valid token before accessing the api")
                response.status_code = 401
                return response
        elif verify_auth_token(session['api_session_token']) == "Success":
            args = request.args.get('top', -1, type = int)
            if(args >= 0):
                return flask.jsonify(result = topJson("open", args))
            else:
                return flask.jsonify(result = toJson("open"))
        else:
            response = jsonify("Please ensure that you have a valid token before accessing the api")
            response.status_code = 401
            return response 

class listCloseOnly(Resource):
    def get(self):
        if(request.authorization != None):
            token = request.authorization.get("username").encode("utf-8", errors = "ignore")
            if(verify_auth_token(token) == "Success"):
                return flask.jsonify(result = toJson("close"))
            else:
                response = jsonify("Please ensure that you have a valid token before accessing the api")
                response.status_code = 401
                return response
        elif verify_auth_token(session['api_session_token']) == "Success":
            args = request.args.get('top', -1, type = int)
            if(args >= 0):
                return flask.jsonify(result = topJson("close", args))            
            else:
                return flask.jsonify(result = toJson("close"))
        else:
            response = jsonify("Please ensure that you have a valid token before accessing the api")
            response.status_code = 401
            return response 

class listAllCsv(Resource):
    def get(self):
        if(request.authorization != None):
            token = request.authorization.get("username").encode("utf-8", errors = "ignore")
            if(verify_auth_token(token) == "Success"):
                bothTimes = toJson("both")
                file = open("times.csv", "w")
                writer = csv.writer(file)
                if(len(bothTimes) > 0):
                    writer.writerow(bothTimes[0].keys())
                for item in bothTimes:
                    writer.writerow(item.values())
                file.close()
                return Response(open('times.csv', 'r'), mimetype='text/csv')
            else:
                response = jsonify("Please ensure that you have a valid token before accessing the api")
                response.status_code = 401
                return response
        elif verify_auth_token(session['api_session_token']) == "Success":
            bothTimes = toJson("both")
            file = open("times.csv", "w")
            writer = csv.writer(file)
            if(len(bothTimes) > 0):
                writer.writerow(bothTimes[0].keys())
            for item in bothTimes:
                writer.writerow(item.values())
            file.close()
            return Response(open('times.csv', 'r'), mimetype='text/csv')
        else:
            response = jsonify("Please ensure that you have a valid token before accessing the api")
            response.status_code = 401
            return response 

class listOpenCsv(Resource):
    def get(self):
        if(request.authorization != None):
            token = request.authorization.get("username").encode("utf-8", errors = "ignore")
            if(verify_auth_token(token) == "Success"):
                openTimes = toJson("open")
                file = open("times.csv", "w")
                writer = csv.writer(file)
                if(len(openTimes) > 0):
                    writer.writerow(openTimes[0].keys())
                for item in openTimes:
                    writer.writerow(item.values())
                file.close()
                return Response(open('times.csv', 'r'), mimetype='text/csv')
            else:
                response = jsonify("Please ensure that you have a valid token before accessing the api")
                response.status_code = 401
                return response
        elif verify_auth_token(session['api_session_token']) == "Success":
            args = request.args.get('top', -1, type = int)
            if(args >= 0):
                openTimes = topJson("open", args)            
            else:
                openTimes = toJson("open")
            
            file = open("times.csv", "w")
            writer = csv.writer(file)
            if(len(openTimes) > 0):
                writer.writerow(openTimes[0].keys())
            for item in openTimes:
                writer.writerow(item.values())
            file.close()
            return Response(open('times.csv', 'r'), mimetype='text/csv')
        else:
            response = jsonify("Please ensure that you have a valid token before accessing the api")
            response.status_code = 401
            return response 

class listCloseCsv(Resource):
    def get(self):
        if(request.authorization != None):
            token = request.authorization.get("username").encode("utf-8", errors = "ignore")
            if(verify_auth_token(token) == "Success"):
                closeTimes = toJson("close")
                file = open("times.csv", "w")
                writer = csv.writer(file)
                if(len(closeTimes) > 0):
                    writer.writerow(closeTimes[0].keys())
                for item in closeTimes:
                    writer.writerow(item.values())
                file.close()
                return Response(open('times.csv', 'r'), mimetype='text/csv')
            else:
                response = jsonify("Please ensure that you have a valid token before accessing the api")
                response.status_code = 401
                return response
        elif verify_auth_token(session['api_session_token']) == "Success":
            args = request.args.get('top', -1, type = int)
            if(args >= 0):
                closeTimes = topJson("close", args)
            else:
                closeTimes = toJson("close")
            
            file = open("times.csv", "w")
            writer = csv.writer(file)
            if(len(closeTimes) > 0):
                writer.writerow(closeTimes[0].keys())
            for item in closeTimes:
                writer.writerow(item.values())
            file.close()
            return Response(open('times.csv', 'r'), mimetype='text/csv')
        else:
            response = jsonify("Please ensure that you have a valid token before accessing the api")
            response.status_code = 401
            return response 

# Create routes
# Another way, without decorators

def toJson(time):
    times = []
    _items = db.tododb.find()
    items = [item for item in _items]
    if(time == 'open'):
        for value in items:
            times.append({
                'open':value['open']
                })
    elif(time == 'close'):
        for value in items:
            times.append({
                'close':value['close']
                })
    elif(time == 'both'):
        for value in items:
            times.append({
                'open':value['open'],
                'close':value['close']
                })
    else:
        app.logger.debug("Enter a valid value")
    return times

def topJson(time, value):
    times = []
    _items = db.tododb.find()
    items = [item for item in _items]
    if(value > len(items)):
        value = len(items) #set value equal to number of times if top request is greater than number of items

    if(time == "open"):
        for x in range(value):
            times.append({
                'open':items[x]['open']
                })

    elif(time == "close"):
        for x in range(value):
            times.append({
                'close':items[x]['close']
                })
    else:
        app.logger.debug("please enter a valid value")
    return times


api.add_resource(listAll, '/listAll', '/listAll/json')
api.add_resource(listOpenOnly, '/listOpenOnly', '/listOpenOnly/json')
api.add_resource(listCloseOnly, '/listCloseOnly', '/listCloseOnly/json')
api.add_resource(listAllCsv, '/listAll/csv')
api.add_resource(listOpenCsv, '/listOpenOnly/csv')
api.add_resource(listCloseCsv, '/listCloseOnly/csv')        

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
