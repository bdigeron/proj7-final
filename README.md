# Project 7: Adding authentication and user interface to brevet time calculator service

Author:Bryce Di Geronimo
Email:bdigeron@uoregon.edu
Repo:https://bitbucket.org/bdigeron/proj7-final
Description: Building on top of project 5 with the brevets calculator, this project implements an API to return the open and close times in JSON and csv format and a consumer program that exposes the API. 

Workflow:
Go to localhost:5000. This will redirect you to the login page. You then need to click on the register link or go to 
localhost:5000/register to create a new user. When registering, if the username is already taken, the page will refresh and you must submit a valid username and password. The minimum length is 3 for both fields. To login, go to localhost:5000/login. Once logged in, you will have access to the brevets calculator which will be on localhost:5000 or localhost:5000/index. Once you have registered, you are automatically logged in and given a token. Also, when you login, you will be given a token. You will need to be logged in to access localhost:5000/api/token to get a token and your token will be stored in your session. In order to access the API's, you will need a valid token. To logout, scroll down and click the logout button.

USING CURL:
To register a user: curl -i -X POST -H "Content-Type: application/json" -d '{"username":"bryce1","password":"bryce1"}' http://127.0.0.1:5000/api/register

HTTP/1.0 201 CREATED
Content-Type: application/json
location: localhost:5000/api/users/1
Set-Cookie: session=.eJwl0UuP2jAABOC_UvnMwc844YgEtNnF20Bsh1yQnwqbQCySrdCu9r83aq-fRpqR5gtc4iNMHVjPj4-wAperB2uAMaPGW5QhmxeceugD566wsTA2dzAPETlc5My7QAseMSOZCThm0QWfQ7YAc4bCgucB55RTgzJKIzecMOgM5NESHilyOfIWY2i4xQTZgC00NMKCgBUw6XqZwjRdx_tlHvtwB-sv8MMu49pmeDH78ucZD7-NTKVqkgpblE6NKK1-ju1WlKJHUtUbEW4iq-VxE3qUPB6M3nfkpNOfsGVSNultcVLfkgi6Pb7eFDvdN6aCPV4yO_PP_Lury6iG8fMw7MaDqpC7D-lVlVWDJDvD7kVA-TRNT-q9eNZkU0k5_6qk0mdVIEXa9qi7Su-K51n72nx2mbgtvfuc2EZ0Wvezg206aMdc7TMhZwK-V-BjCo__VyDw_Rddo4QQ.XANzcA.RWvoFbPDemYJZJ4W9ZQedZ97dTM; HttpOnly; Path=/
Content-Length: 27
Server: Werkzeug/0.14.1 Python/3.7.1
Date: Sun, 02 Dec 2018 05:53:52 GMT

{
  "username": "bryce1"
}


To get a token: curl -u bryce1:bryce1 -i -X GET http://127.0.0.1:5000/api/token

{
  "duration": 600, 
  "token": "eyJhbGciOiJIUzUxMiIsImlhdCI6MTU0MzczMDE1NSwiZXhwIjoxNTQzNzMwNzU1fQ.eyJpZCI6IjEifQ.a5aFcwZQSDLEt7ZlKQyZzIxrf-KJn7xJGfGVPtno6FfkoJszm8CNvGYc_UaXx48MlFt_2zgue5jowZ9mhCAQnw"
}


To access a restricted resource, use the token as a username and use pass as a placeholder. curl -u "eyJhbGciOiJIUzUxMiIsImlhdCI6MTU0MzcyODQzNSwiZXhwIjoxNTQzNzI5MDM1fQ.eyJpZCI6IjEifQ.SP9lmac2DS-KjbptmrPqLARbjrOVIUkoGfQRdeKI8IAI1a2ir3R97kSBgnXSn0aY0__y_3UsLS6xGgSb0Neq8Q":pass -i -X GET http://127.0.0.1:5000/listAll

{
  "result": [
    {
      "close": "Sun 1/1 0:06", 
      "open": "Sun 1/1 0:02"
    }, 
    {
      "close": "Sun 1/1 0:12", 
      "open": "Sun 1/1 0:05"
    }
  ]
}



Notables:
Currently there are no checks for logging in multiple times as the same user. Once you are registered or logged in, you can go to the login page and login again.  


API calls
    * "http://<host:port>/listAll" should return all open and close times in the database
    * "http://<host:port>/listOpenOnly" should return open times only
    * "http://<host:port>/listCloseOnly" should return close times only
    * "http://<host:port>/listAll/csv" should return all open and close times in CSV format
    * "http://<host:port>/listOpenOnly/csv" should return open times only in CSV format
    * "http://<host:port>/listCloseOnly/csv" should return close times only in CSV format
    * "http://<host:port>/listAll/json" should return all open and close times in JSON format
    * "http://<host:port>/listOpenOnly/json" should return open times only in JSON format
    * "http://<host:port>/listCloseOnly/json" should return close times only in JSON format
    * "http://<host:port>/listOpenOnly/csv?top=3" should return top 3 open times only (in ascending order) in CSV format 
    * "http://<host:port>/listOpenOnly/json?top=5" should return top 5 open times only (in ascending order) in JSON format
    * "http://<host:port>/listCloseOnly/csv?top=6" should return top 5 close times only (in ascending order) in CSV format
    * "http://<host:port>/listCloseOnly/json?top=4" should return top 4 close times only (in ascending order) in JSON format






